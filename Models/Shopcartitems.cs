﻿using System;
using System.Collections.Generic;

namespace ShopBridge.Models
{
    public partial class Shopcartitems
    {
        public int Id { get; set; }
        public string Itemname { get; set; }
        public string Description { get; set; }
        public int? Price { get; set; }
    }
}
