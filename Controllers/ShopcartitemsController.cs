﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShopBridge.Models;

namespace ShopBridge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShopcartitemsController : ControllerBase
    {
        private readonly ShopBridgeContext _context;

        public ShopcartitemsController(ShopBridgeContext context)
        {
            _context = context;
        }

        // GET: api/Shopcartitems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Shopcartitems>>> GetShopcartitems()
        {
            return await _context.Shopcartitems.ToListAsync();
        }

        // GET: api/Shopcartitems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Shopcartitems>> GetShopcartitems(int id)
        {
            var shopcartitems = await _context.Shopcartitems.FindAsync(id);

            if (shopcartitems == null)
            {
                return NotFound();
            }

            return shopcartitems;
        }

        // PUT: api/Shopcartitems/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutShopcartitems(int id, Shopcartitems shopcartitems)
        {
            if (id != shopcartitems.Id)
            {
                return BadRequest();
            }

            _context.Entry(shopcartitems).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShopcartitemsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Shopcartitems
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Shopcartitems>> PostShopcartitems(Shopcartitems shopcartitems)
        {
            _context.Shopcartitems.Add(shopcartitems);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetShopcartitems", new { id = shopcartitems.Id }, shopcartitems);
        }

        // DELETE: api/Shopcartitems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Shopcartitems>> DeleteShopcartitems(int id)
        {
            var shopcartitems = await _context.Shopcartitems.FindAsync(id);
            if (shopcartitems == null)
            {
                return NotFound();
            }

            _context.Shopcartitems.Remove(shopcartitems);
            await _context.SaveChangesAsync();

            return shopcartitems;
        }

        private bool ShopcartitemsExists(int id)
        {
            return _context.Shopcartitems.Any(e => e.Id == id);
        }
    }
}
